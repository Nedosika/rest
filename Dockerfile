FROM node:13

#RUN git clone https://gitlab.com/Nedosika/rest.git /app

WORKDIR /app

# Установить зависимости приложения
# Используется символ подстановки для копирования как package.json, так и package-lock.json,
# работает с npm@5+
COPY package*.json ./

RUN npm install
# Используется при сборке кода в продакшене
# RUN npm install --only=production

COPY src /app

EXPOSE 3000
CMD [ "node", "index.js" ]

