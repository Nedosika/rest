const Sequelize = require("sequelize");
const express = require("express");
const bodyParser = require("body-parser");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");

const app = express();
const urlencodedParser = bodyParser.urlencoded({extended: false});

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'REST Full API',
            description: 'REST Full API Information!',
            contact: {
                name: 'PaNed Developer '
            },
        },
        schemes: ['http'],
        host: 'localhost:3000',
        basePath: '/api'
    },
    apis: ['index.js']
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-doc", swaggerUI.serve, swaggerUI.setup(swaggerDocs));

// определяем объект Sequelize
const sequelize = new Sequelize("nodejs", "nodejs", "lg23GrT48aClbUfW", {
    dialect: "mysql",
    host: "192.168.0.200",
    define: {
        timestamps: false
    }
});

// определяем модель Message
const Message = sequelize.define("message", {
    mid: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    message: {
        type: Sequelize.STRING,
        allowNull: false
    },
    uid_fk: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});
app.set("view engine", "hbs");

// синхронизация с бд, после успшной синхронизации запускаем сервер
sequelize.sync().then(()=>{
    app.listen(3000, function(){
        console.log("Сервер ожидает подключения...");
    });
}).catch(err => console.log(err));

/**
* @swagger
* /messages:
*   get:
*     description: Use to request all messages
*     responses:
*       '201':
*         description: Asuccessful response
*/
app.get("/", function(req, res){
Message.findAll({raw: true }).then(data=>{
        res.json(data);
    }).catch(err => console.log(err));
});

// добавление данных
/**
* @swagger
* /messages:
*   post:
*     description: Use to create messages
*     responses:
*       '201':
*         description: Asuccessful response
*/
app.post("/create", urlencodedParser, function (req, res) {
    if(!req.body) return res.sendStatus(400);
    const {message, uid} = req.body;
    Message.create({ message, uid_fk: uid})
        .then((data) => res.json(data))
        .catch(err => console.log(err));
});

// получаем объект по id для редактирования
app.get("/edit/:id", function(req, res){
    const {id} = req.params;
    Message.findAll({where:{mid: id}, raw: true })
        .then(data => {
            res.json(data)
        })
        .catch(err => console.log(err));
});

// обновление данных в БД
app.post("/edit", urlencodedParser, function (req, res) {
    if(!req.body) return res.sendStatus(400);
    const {mid, message, uid} = req.body;
    Message.update({mid, message, uid_fk: uid}, {where: {mid}}).then((data) => {
        res.json(data);
    })
    .catch(err => console.log(err));
});

// удаление данных
app.post("/delete/:id", function(req, res){
    const {mid} = req.params;
    Message.destroy({where: {mid} }).then((data) => {
        res.json(data);
    }).catch(err => console.log(err));
});
